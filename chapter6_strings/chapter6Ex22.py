###################################################################################################
# 22.                                                                                             #
# A simple way of encrypting a message is to rearrange its characters. One way                    #
# to rearrange the characters is to pick out the characters at even indices,                      #
# put them first in the encrypted string, and follow them by the odd characters.                  #
# For example, the string message would be encrypted as msaeesg because the even                  #
# characters are m, s, a, g (at indices 0, 2, 4, and 6) and the odd characters                    #
# are e, s, g (at indices 1, 3, and 5).                                                           #
# (a) Write a program that asks the user for a string and uses this method to encrypt the string. #
# (b) Write a program that decrypts a string that was encrypted with this method.                 #
###################################################################################################

user_input = input("Please, enter a word for encryption: ")
###########
# encript #
###########
encrypted_odd_message =''
encrypted_even_message =''
for i in range(len(user_input)):
	if i%2==0:
		encrypted_odd_message = encrypted_odd_message + user_input[i]
	else:
		encrypted_even_message = encrypted_even_message + user_input[i]
print("ENCRYPTED message [",encrypted_odd_message+encrypted_even_message,"]")


############
# decrypts #
############
user_input = encrypted_odd_message+encrypted_even_message  # mock to try decryption

# user_input = input("Please, enter a word for decryption: ")
odd_part =''
even_part =''
for i in range(len(user_input)):
	# print(">>>>>>>>>>>>>>>>>> i [",i," ] XX user_input [",user_input[i],"] len(user_input) [",len(user_input)/2,"]<<<<<<<<<<<<<<<<<<<<<")
	if i < (len(user_input)/2):
		# print("\tA>>>>>>>>>>>>>>>>>>",i,"XX",user_input[i],"<<<<<<<<<<<<<<<<<<<<<")
		odd_part = odd_part + user_input[i]
	else:
		# print("\tB>>>>>>>>>>>>>>>>>>",i,"XX",user_input[i],"<<<<<<<<<<<<<<<<<<<<<")
		even_part = even_part + user_input[i]
print("DECRYPTED message [ ",end='')

print("len even_part [",len(even_part),"] len odd_part [",len(odd_part),"]")

for i in range(len(even_part)):
	print(odd_part[i],even_part[i],end="",sep="")
if len(even_part) != len(odd_part):
	print("distintos")
else:
	print("Not dis")
print(" ]",i)
