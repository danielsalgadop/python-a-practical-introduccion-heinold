#################################################################################
# 21.                                                                           #
# An anagram of a word is a word that is created by rearranging the letters of  #
# the original. For instance, two anagrams of idle are deli and lied. Finding   #
# anagrams that are real words is beyond our reach until Chapter 12. Instead,   #
# write a program that asks the user for a string and returns a random anagram  #
# of the string—in other words, a random rearrangement of the letters of that   #
# string.                                                                       #
#################################################################################
# I DONT KNOW HOW TO DO IT WITH OUT WHILES
from random import randint
word_input = input("Please, insert a word: ")

# debug
# word_input="123qwe"


anagram = ""

word_input_length = len(word_input)
# # construir anagram con caracter especial ( para saber si ya esta cambiado
for i in range(0,word_input_length):
	anagram = anagram + ")"
# recorrer word_input
for i in range(0,word_input_length):
	letter_to_randomice = word_input[i]


	letter_in_anagram = ""
	continue_searching = 1

    # VERY VERY bad way to do it! this is a mock WHILE
	for ii in range(1,10000):
		random_position = randint(0,word_input_length-1)
		letter_in_anagram = anagram[random_position];

		if letter_in_anagram == ")" and continue_searching == 1: # 	estaba vacia

            # replace 1 position of anangram
			anagram = anagram[:random_position]+letter_to_randomice+anagram[random_position+1:]
			# no need to continue searching
			continue_searching = 0
			# print("letter_to_randomice [",letter_to_randomice,"] value of i [",i,"] random_position [",random_position,"] letter_in_anagram [",letter_in_anagram,"]")

print("anagram [",anagram,"]")