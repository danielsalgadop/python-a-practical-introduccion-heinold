################################################################################
# 13.                                                                          #
# Write a program that asks the user to enter two strings of the same length.  #
# The program should then check to see if the strings are of the same length.  #
# If they are not, the program should print an appropriate message and exit.   #
# If they are of the same length, the program should alternate the characters  #
# of the two strings. For example, if the user enters abcde and ABCDE the      #
# program should print out AaBbCcDdEe.                                         #
################################################################################

user_string_1 = input("Please enter first string: ")
user_string_2 = input("Please enter second string, it must be the same length than the fist one: ")
if len(user_string_1) != len(user_string_2):
	print("the string ARE NOT of equal length")
else:
	mixed_string = ''
	for i in range(len(user_string_1)):
		mixed_string = mixed_string + user_string_1[i] + user_string_2[i]
	print(mixed_string)