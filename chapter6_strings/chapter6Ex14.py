#################################################################################
# 14.                                                                           #
# Write a program that asks the user to enter their name in lowercase and then  #
# capitalizes the first letter of each word of their name.                      #
#################################################################################

user_names = input("Please, enter your name in lowercase: ")
next_letter_to_capitalize = 0
capitalized_name = ''

for i in range(len(user_names)):
	letter = user_names[i]
	if letter == " ":
		next_letter_to_capitalize = 1
	# we are in letter to capitalize
	elif next_letter_to_capitalize == 1 or i == 0:
		letter = letter.upper()
		next_letter_to_capitalize = 0
	capitalized_name = capitalized_name + letter

print(capitalized_name)