################################################################################
# 17.                                                                          #
# Write a program that generates the 26-line block of letters partially shown  #
# below. Use a loop containing one or two print statements.                    #
# abcdefghijklmnopqrstuvwxyz                                                   #
# bcdefghijklmnopqrstuvwxyza                                                   #
# cdefghijklmnopqrstuvwxyzab                                                   #
# ...                                                                          #
# yzabcdefghijklmnopqrstuvwx                                                   #
# zabcdefghijklmnopqrstuvwxy                                                   #
################################################################################
alphabet = "abcdefghijklmnopqrstuvwxyz"
for i in range(26):
	one_line=''
	for z in range(len(alphabet)):
		# alphabet_index = z+i%len(alphabet)
		alphabet_index = z+i
		if alphabet_index >= len(alphabet):
			alphabet_index = alphabet_index - len(alphabet)
		one_line = one_line + alphabet[alphabet_index]
	print(one_line)
