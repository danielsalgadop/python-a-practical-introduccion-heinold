##################################################################################
# 18.                                                                            #
# The goal of this exercise is to see if you can mimic the behavior of the in    #
# operator and the count and index methods using only variables, for loops, and  #
# if statements.                                                                 #
# (a) Without using the in operator, write a program that asks the user for a    #
# string and a letter and prints out whether or not the letter appears in the    #
# string.                                                                        #
# (b) Without using the count method, write a program that asks the user for     #
# a string and a letter and counts how many occurrences there are of the letter  #
# in the string.                                                                 #
# (c) Without using the index method, write a program that asks the user for a   #
# string and a letter and prints out the index of the first occurrence of the    #
# letter in the string. If the letter is not in the string, the program should   #
# say so.                                                                        #
##################################################################################
user_string = input("Please, type a string: ")
user_letter = input("Please, type a letter: ")

# a and b

flag_and_counter_letter_in_string = 0
first_index = -1
for i in range(len(user_string)):
	if user_string[i] == user_letter:
		flag_and_counter_letter_in_string = flag_and_counter_letter_in_string + 1
		if first_index == -1:
			first_index = i

if flag_and_counter_letter_in_string > 0:
	print("MATCH!, letter [",user_letter,"] inside string [",user_string,"] counter [",flag_and_counter_letter_in_string,"] the first occurrence index is [",first_index+1,"]")
else:
	print("NO MATCH")