##################################################################################
# 7.                                                                             #
# Write a program that asks the user to enter a word and determines whether the  #
# word is a palindrome or not. A palindrome is a word that reads the same        #
# backwards as forwards.                                                         #
##################################################################################
is_it_a_palindrome = input("Please, enter a word: ")
flag_it_is_a_palindrome = 1
for i in range(round(len(is_it_a_palindrome)/2)):
	# print("i[",i,"] -i[",-i,"]")
	print(is_it_a_palindrome[i]," ",is_it_a_palindrome[-i-1])
	if is_it_a_palindrome[i] != is_it_a_palindrome[-i-1]:
		flag_it_is_a_palindrome = 0

if flag_it_is_a_palindrome == 1:
	print("It is a palindrome")
else:
	print("It is NOT a palindrome")