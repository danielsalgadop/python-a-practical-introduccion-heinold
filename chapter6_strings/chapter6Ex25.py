###################################################################################
# 25.                                                                             #
# In algebraic expressions, the symbol for multiplication is often left out, as   #
# in 3x+4y or 3(x+5) .Computers prefer those expressions to include the           #
# multiplication symbol, like 3*x+4*y or 3*(x+5) . Write a program that asks the  #
# user for an algebraic expression and then inserts multiplication symbols        #
# where appropriate.                                                              #
###################################################################################

user_expression = input("an algebraic expressions: ")

# debug/test passed
# user_expression = "3x+4y(5+4)"
# user_expression = "(4+4)/3x4y"

final_expression = ""

for i in range(len(user_expression)):
	letter_in_expression = user_expression[i]
	print("i [",i,"] letter [",letter_in_expression,"]",end='')
	concatenated_string = ''
	# if letter_in_expression is alphabet or is "(" but not first character, prepend *
	if letter_in_expression.isalpha() or (letter_in_expression == "(" and  i != 0):
		concatenated_string = "*"+letter_in_expression
		print("YES",end='');
	else:
		concatenated_string = letter_in_expression

	final_expression = final_expression + concatenated_string
	print("\n")
	# user_expression = user_expression.replace("","*")
	# user_expression = user_expression.replace("","*")
print("final_expression [",final_expression,"]")