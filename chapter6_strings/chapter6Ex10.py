###################################################################################
# 10.                                                                             #
# Write a program that asks the user to enter a string, then prints out each      #
# letter of the string doubled and on a separate line. For instance, if the user  #
# entered HEY , the output would be                                               #
# HHH                                                                             #
# EEE                                                                             #
# YYY                                                                             #
###################################################################################
user_word = input("Please, enter a word: ")
for i in range(len(user_word)):
	print(user_word[i]*3)