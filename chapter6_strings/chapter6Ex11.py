####################################################################################
# 11.                                                                              #
# Write a program that asks the user to enter a word that contains the letter a.   #
# The program should then print the following two lines: On the first line         #
# should be the part of the string up to and including the the first a, and on     #
# the second line should be the rest of the string. Sample output is shown below:  #
# Enter a word: buffalo                                                            #
# buffa                                                                            #
# lo                                                                               #
####################################################################################
user_word = input("Please, enter a word containing letter 'a': ")
if user_word.count('a') >= 1:
	letter_a_index =  user_word.index("a")
	print(user_word[:letter_a_index+1])
	print(user_word[letter_a_index+1:])
else:
	print("there is no letter 'a'")