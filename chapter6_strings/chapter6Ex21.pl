#!/usr/bin/perl
use strict;
use warnings;
#################################################################################
# 21.                                                                           #
# An anagram of a word is a word that is created by rearranging the letters of  #
# the original. For instance, two anagrams of idle are deli and lied. Finding   #
# anagrams that are real words is beyond our reach until Chapter 12. Instead,   #
# write a program that asks the user for a string and returns a random anagram  #
# of the string—in other words, a random rearrangement of the letters of that   #
# string.                                                                       #
#################################################################################

# uncomment 2 lines to real user_input
# my $word_input = <>;
# chomp($word_input);

# debug: no real user_input
my $word_input = "123qwe";

print "word_input [" . $word_input . "]\n";
my $anagram = "";

my $word_input_length = length($word_input);

# # construir anagram con caracter especial ( para saber si ya esta cambiado
foreach my $i ( 0 ... $word_input_length-1 ) {
    $anagram .= ")";
}
# recorrer word_input
foreach my $i ( 0 ... $word_input_length-1 ) {
    my $letter_to_randomice = substr( $word_input, $i, 1 );

    my $letter_in_anagram;
    my $continue_searching = 1;

    # VERY VERY bad way to do it! this is a mock WHILE
    for (1..10000){
        my $random_position = int( rand($word_input_length) );
        $letter_in_anagram = substr( $anagram, $random_position, 1 );

        if ( $letter_in_anagram eq ")"  and $continue_searching) { #    estaba vacia

            # replace 1 position of anangram
            substr( $anagram, $random_position, 1, $letter_to_randomice );
            # no need to continue searching
            $continue_searching = 0;
        }
    }

# print "\n=============2i[".$i."]"."letter_to_randomice[".$letter_to_randomice."]"."random_position[".$random_position."]";
}
print "\nanagram [" . $anagram . "]\n";