###################################################################################
# 8.                                                                              #
# At a certain school, student email addresses end with @student.college.edu ,    #
# while professor email addresses end with @prof.college.edu . Write a program    #
# that first asks the user how many email addresses they will be entering, and    #
# then has the user enter those addresses. After all the email addresses are      #
# entered, the program should print out a message indicating either that all the  #
# addresses are student addresses or that there were some professor addresses     #
# entered.                                                                        #
###################################################################################
number_of_emails = eval(input("Please, enter number of mails: "))
concatenation_of_mails = ''
for i in range(number_of_emails):
	concatenation_of_mails = concatenation_of_mails + input("Enter a mail number: ")

number_of_professor_mails = concatenation_of_mails.count("@prof.college.edu")

if number_of_professor_mails >= 1:
	print("There are [",number_of_professor_mails,"] of professor mails")
else:
	print("100% student mails")