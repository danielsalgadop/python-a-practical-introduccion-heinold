#################################################################################
# 24.                                                                           #
# In calculus, the derivative of x^4 is 4x^3 . The derivative of x^5 is 5x^4.   #
# The derivative of x^6 is 6x^5 . This pattern continues. Write a program that  #
# asks the user for input like x^3 or x^25 and prints the derivative. For       #
# example, if the user enters x^3 , the program should print out 3x^2.          #
#################################################################################
# Using type conversion int() and str() (not seen in book yet)

to_derivate = input("insert 'formula' to derivate ")
elevated_to = to_derivate[-1:]
final = ""

print("elevated_to [",elevated_to,"]")
minus_one = int(elevated_to) - 1
final = elevated_to+"x^"+str(minus_one)
print("final [",final,"])