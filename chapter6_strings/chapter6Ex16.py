##################################################################################
# 16.                                                                            #
# Companies often try to personalize their offers to make them more attractive.  #
# One simple way to do this is just to insert the person’s name at various       #
# places in the offer. Of course, companies don’t manually type in every         #
# person’s name; everything is computer-generated. Write a program that asks     #
# the user for their name and then generates an offer like the one below. For    #
# simplicity’s sake, you may assume that the person’s first and last names are   #
# one word each.                                                                 #
# Enter name: George Washington												 	 #
# Dear George Washington,														 #
# I am pleased to offer you our new Platinum Plus Rewards card					 #
# at a special introductory APR of 47.99%. George, an offer					     #
# like this does not come along every day, so I urge you to call				 #
# now toll-free at 1-800-314-1592. We cannot offer such a low					 #
# rate for long, George, so call right away.					 				 #
##################################################################################
name_and_second_name = input("Please enter first and second name: ")
index_to_space = name_and_second_name.index(" ")
name = name_and_second_name[:index_to_space]

print("Dear",name_and_second_name)
print("I am pleased to offer you our new Platinum Plus Rewards card")
print("at a special introductory APR of 47.99%.",name,", an offer", sep='')
print("like this does not come along every day, so I urge you to call")
print("now toll-free at 1-800-314-1592. We cannot offer such a low")
print("rate for long, ",name,", so call right away,", sep='')
