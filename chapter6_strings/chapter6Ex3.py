#############################################################################
# 3.                                                                        #
# People often forget closing parentheses when entering formulas. Write a   #
# program that asks the user to enter a formula and prints out whether the  #
# formula has the same number of opening and closing parentheses.           #
#############################################################################
formula = input("Please enter a formula :")
if formula.count("(") == formula.count(")"):
	print("formula OK")
else:
	print("missing parentheses")