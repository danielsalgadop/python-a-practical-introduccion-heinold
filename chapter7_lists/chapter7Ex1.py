##############################################################################################
# 1.                                                                                         #
# Write a program that asks the user to enter a list of integers. Do the following:          #
# (a) Print the total number of items in the list.                                           #
# (b) Print the last item in the list.                                                       #
# (c) Print the list in reverse order.                                                       #
# (d) Print Yes if the list contains a 5 and No otherwise.                                   #
# (e) Print the number of fives in the list.                                                 #
# (f) Remove the first and last items from the list, sort the remaining items, and print the #
# result.                                                                                    #
# (g) Print how many integers in the list are less than 5.                                   #
##############################################################################################
# user_list_of_integers = eval(input( ' Enter a list of integers (dont forget to use [ and ]: ' ))
# debug
user_list_of_integers = [0,2,3,4,5,99]
# print(user_list_of_integers.reverse())

#####
# a #
#####
print( 'total number of items [',len(user_list_of_integers),']')

#####
# b #
#####
print( 'last item [',user_list_of_integers[-1],']')

#####
# c #
#####
user_list_of_integers.reverse()
print( 'reverse order ',user_list_of_integers,'')

#####
# d #
#####
print( 'has a 5 inside? ',end='')
if 5 in user_list_of_integers:
	print("YES!")
else:
	print("NO number 5")

#####
# e #
#####
print( 'number of 5\'s [',user_list_of_integers.count(5),']')

#####
# f #
#####
user_list_of_integers.pop(0) # remove first element
user_list_of_integers.pop(-1) # remove last elemet
user_list_of_integers.sort() # sort list
print('sorted list ', user_list_of_integers)

#####
# g #
#####
number_of_items_smaller_than_5 = 0
for i in user_list_of_integers:
	if i < 5:
		number_of_items_smaller_than_5 = number_of_items_smaller_than_5 + 1
print("number_of_items_smaller_than_5 [",number_of_items_smaller_than_5,"]")

