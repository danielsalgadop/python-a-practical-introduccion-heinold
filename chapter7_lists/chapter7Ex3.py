############################################################
# 3.                                                       #
# Start with the list [8,9,10] . Do the following:         #
# (a) Set the second entry (index 1) to 17                 #
# (b) Add 4, 5, and 6 to the end of the list               #
# (c) Remove the first entry from the list                 #
# (d) Sort the list                                        #
# (e) Double the list                                      #
# (f) Insert 25 at index 3                                 #
# The final list should equal [4,5,6,25,10,17,4,5,6,10,17] #
############################################################
starting_list = [8,9,10]

#####
# a #
#####
starting_list[1] = 17

#####
# b #
#####
# starting_list.append(4)
# starting_list.append(5)
# starting_list.append(6)

starting_list =  starting_list+[4,5,6]

#####
# c #
#####
starting_list.pop(0)

#####
# d #
#####
starting_list.sort()

#####
# e #
#####
starting_list = starting_list * 2

#####
# f #
#####
starting_list.insert(3,25)


#################################
# Control, has it gone correct? #
#################################
print("Exercise is: ",end='')
if starting_list == [4,5,6,25,10,17,4,5,6,10,17]:
	print("CORRECT!!")
else:
	print("INCORRECT")

print("we have create this list ",starting_list)