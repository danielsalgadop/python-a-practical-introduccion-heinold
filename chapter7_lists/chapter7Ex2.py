#################################################################################
# 2.                                                                            #
# Write a program that generates a list of 20 random numbers between 1 and 100. #
# (a) Print the list.                                                           #
# (b) Print the average of the elements in the list.                            #
# (c) Print the largest and smallest values in the list.                        #
# (d) Print the second largest and second smallest entries in the list          #
# (e) Print how many even numbers are in the list.                              #
#################################################################################
from random import randint
list_of_random_numbers = []
for i in range(21):
	list_of_random_numbers.append(randint(1,100))

#####
# a #
#####
print(list_of_random_numbers)

#####
# b #
#####
print('average of elements [',sum(list_of_random_numbers)/len(list_of_random_numbers),']')

#####
# c #
#####
print('Largest number [',max(list_of_random_numbers),'] Smallest [',min(list_of_random_numbers),']')

#####
# d #
#####
list_of_random_numbers.sort()
# print(list_of_random_numbers)
print('second largest [',list_of_random_numbers[-2],']  second smallest [',list_of_random_numbers[1],']')

#####
# e #
#####
count_of_even_numbers = 0
for i in list_of_random_numbers:
	if i%2 != 0:
		count_of_even_numbers = count_of_even_numbers + 1
print('there are [',count_of_even_numbers,'] of even numbers in list')