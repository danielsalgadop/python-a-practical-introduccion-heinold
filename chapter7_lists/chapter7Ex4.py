############################################################################
# 4.                                                                       #
# Ask the user to enter a list containing numbers between 1 and 12. Then   #
# replace all of the entries in the list that are greater than 10 with 10. #
############################################################################
print('enter a list containing numbers between 1 and 12')
user_list = input().split(',')
for i in range(len(user_list)):
    value = int(user_list[i])
    if value > 10:
        user_list[i] = '10'
print(user_list)
