################################################################
# 4.                                                           #
# Write a program that generates a random decimal number       #
# between 1 and 10 with two decimal                            #
# places of accuracy. Examples are 1.23, 3.45, 9.80, and 5.00. #
################################################################
from random import *
unit_number = randint(0,9)
decimal_number = random()
# print("decimal_number [",decimal_number,"] unit_number [",unit_number,"]")
total_number = unit_number+decimal_number
print("%.2f" % total_number)
