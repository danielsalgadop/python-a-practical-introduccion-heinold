########################################################
# 14.                                                  #
# Write a program that asks the user to enter an angle #
# in degrees and prints out the sine of that           #
# angle.                                               #
########################################################
from math import sin
users_angle = eval(input("enter an angle"))
print("the sine of ",users_angle," is ",sin(users_angle))