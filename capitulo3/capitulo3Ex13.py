#########################################################
# 13.                                                   #
# Write a program that asks the user for a number and   #
# then prints out the sine, cosine, and tangent of that #
# number.                                               #
#########################################################
from math import sin, cos, tan
user_number = eval(input("a number"))
print("given this number ",user_number," its sine ",sin(user_number)," cosine ",cos(user_number)," tangent ",tan(user_number))