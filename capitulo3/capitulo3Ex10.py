#####################################################################
# 10.                                                               #
# (a)                                                               #
# One way to find out the last digit of a number is to mod the      #
# number by 10. Write a program that asks the user to enter a       #
# power. Then find the last digit of 2 raised to that power.        #
# (b)                                                               #
# One way to find out the last two digits of a number is to mod     #
# the number by 100. Write a program that asks the user to enter    #
# a power. Then find the last two digits of 2 raised to that power. #
# (c)                                                               #
# Write a program that asks the user to enter a power and how many  #
# digits they want. Find the last that many digits of 2 raised to   #
# the power the user entered.                                       #
# ###################################################################

# (a)
user_power = eval(input("enter a power"))
power = 2**user_power
last_number = power%10
print("The last number of 2 raised to ",user_power,"= (",power,") is ",last_number)

# (b)
user_power = eval(input("enter a power"))
power = 2**user_power
last_two_numbers = power%100
print("The last two numbers of 2 raised to ",user_power,"= (",power,") are ",last_two_numbers)

# (c)
user_power = eval(input("enter a power"))
how_many_digits = eval(input("How many digits you want?"))
how_many_digits = 10**how_many_digits
power = 2**user_power
last_two_numbers = power%how_many_digits
print("The last two numbers of 2 raised to ",user_power,"= (",power,") are ",last_two_numbers)