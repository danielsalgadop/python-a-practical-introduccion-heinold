#################################################################
#1.                                                             #
# Write a program that generates and prints 50 random integers, #
# each between 3 and 6.                                         #
#################################################################
from random import randint

for i in range(1,51):
    x = randint(1,10)
    print("A random number between 1 and 10: ",x)