#################################################################################
# 4.                                                                            #
# Write a program that asks the user how many credits they have taken. If they  #
# have taken less than 23, print that the student is a freshman. If they have   #
# taken between 24 and 53, print that they are a sophomore. The range for       #
# juniors is 54 to 83, and seniors are 84 and over.                             #
#################################################################################
taken_user_credits = eval(input("how many credits they have you taken? "))
if taken_user_credits < 23:
	print("freshman")
elif taken_user_credits >= 24 and taken_user_credits <= 53:
	print("sophomore")
elif taken_user_credits >= 54 and taken_user_credits <= 83:
	print("junior")
else:
	print("senior")