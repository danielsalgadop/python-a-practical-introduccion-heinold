###################################################################################
# 8.                                                                              #
# A year is a leap year if it is divisible by 4, except that years divisible      #
# by 100 are not leap years unless they are also divisible by 400. Write a        #
# program that asks the user for a year and prints out whether it is a leap year  #
# or not.                                                                         #
###################################################################################
is_it_a_leap_year = eval(input("write a year, I will tell you if it is a leap year (or not) "))
print(is_it_a_leap_year," is a leap year? ",end="")

if is_it_a_leap_year%4 == 0:
	if is_it_a_leap_year%100 == 0:
		if is_it_a_leap_year%400 == 0:
			print("Yes it is!")
	else:
		print("Yes it is!")
else:
	print("No it is not")
