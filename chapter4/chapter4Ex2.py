##############################################################
# 2.                                                         #
# Ask the user for a temperature. Then ask them what units,  #
# Celsius or Fahrenheit, the temperature is in. Your program #
# should convert the temperature to the other unit. The      #
# conversions are F = 9*C/5 + 32 and C = 5/9  * (F − 32) .   #
##############################################################

temperature = eval(input("a temperature? "))
original_unit = input("celsius (c) or farenheit (f)")

if original_unit == "c":
	print(temperature," Celsius are ",(9*temperature/5)+32," Farenheit")
elif original_unit == "f":
	print(temperature," Farenheit are ",5/9*(temperature- 32)," Celsius")
