#############################################################################
# 13.                                                                       #
# Write a program that lets the user play Rock-Paper-Scissors against the   #
# computer. There should be five rounds, and after those five rounds, your  #
# program should print out who won and lost or that there is a tie.         #
#############################################################################
from random import randint

print("lets play Rock-Paper-Scissors!")


# Scores of players
user_score = 0
computer_score = 0

# Screen messages
tail_message_for_both = "wins this round!!!!!!!!!!!!!!!!!!!!!!!!!"
message_when_user_wins_round = "user " + tail_message_for_both
message_when_computer_wins_round = "computer " + tail_message_for_both

for i in range(5):
	user_choice = eval(input("\n==================\nWhat do you choose Rock(1) Paper(2) Scissors(3)"))
	computer_choice = randint(1,3)

	########################
	#print Computer Choice #
	########################
	print("computer chooses ",end="")
	if computer_choice == 1:
		print("Rock")
	elif computer_choice == 2:
		print("Paper")
	else:
		print("Scissors")

	################################
	# Evaluate who wins this round #
	################################
	if user_choice != computer_choice:
		# user Rock - computer Paper
		if user_choice == 1 and computer_choice == 2:
			computer_score = computer_score + 1
			print(message_when_computer_wins_round)
		# user Rock - computer Scissors
		elif user_choice == 1 and computer_choice == 3:
			user_score = user_score + 1
			print(message_when_user_wins_round)
		# user Paper - computer Scissors
		elif user_choice == 2 and computer_choice == 3:
			computer_score = computer_score + 1
			print(message_when_computer_wins_round)
		# user Scissors - computer Rock
		elif user_choice == 3 and computer_choice == 1:
			computer_score = computer_score + 1
			print(message_when_computer_wins_round)
		# user Scissors - computer Paper
		elif user_choice == 3 and computer_choice == 2:
			user_score = user_score + 1
			print(message_when_user_wins_round)


		# computer Rock - user Paper
		elif computer_choice == 1 and user_choice == 2:
			user_score = user_score + 1
			print(message_when_user_wins_round)
		# computer Rock - computer Scissors
		elif computer_choice == 1 and user_choice == 3:
			computer_score = computer_score + 1
			print(message_when_computer_wins_round)
	else:
		print("we have a draw in this round!")

# "What do you choose Rock(1) Paper(2) Scissors(3)"))

###############################
# Evaluate Who wins the match #
###############################
if user_score == computer_score:
	print("he have a draw, both have this score [",user_score,"]")
elif user_score > computer_score:
	print("USER WINS MATCH Score [",user_score,"] computer score [",computer_score,"]")
else:
	print("COMPUTER WINS MATCH [",computer_score,"] user score [",user_score,"]")