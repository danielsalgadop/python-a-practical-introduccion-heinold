################################################################################
# 5.                                                                           #
# Generate a random number between 1 and 10. Ask the user to guess the number  #
# and print a message based on whether they get it right or not.               #
################################################################################
from random import randint
random_number = randint(1,10)
user_guess = eval(input("please, try to guess the random number generated between 1 - 10 "))
if user_guess == random_number:
	print("you are right! this is the number ",random_number)
else:
	print("sorry you guessed [",user_guess,"] the random_number was [",random_number,"]")