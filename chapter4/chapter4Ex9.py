##################################################################################
# 9.                                                                             #
# Write a program that asks the user to enter a number and prints out all the    #
# divisors of that number. [Hint: the % operator is used to tell if a number is  #
# divisible by something]                                                        #
##################################################################################
# EXTRA: if a number is only divisible by 1 or itself it is a prime number, detect
# them
user_number = eval(input("please, enter a number "))
is_it_a_prime_number = 0
for i in range(user_number,0,-1):
	if user_number%i == 0:
		is_it_a_prime_number = is_it_a_prime_number + 1
		print("[",user_number,"] is divisible by ",i)

if is_it_a_prime_number == 2:
	print("It is prime number")
