################################################################################
# 3.                                                                           #
# Ask the user to enter a temperature in Celsius. The program should print a   #
# message based on the temperature:                                            #
# • If the temperature is less than -273.15, print that the temperature is     #
# invalid because it is below absolute zero.                                   #
# • If it is exactly -273.15, print that the temperature is absolute 0.        #
# • If the temperature is between -273.15 and 0, print that the temperature    #
# is below freezing.                                                           #
# • If it is 0, print that the temperature is at the freezing point.           #
# • If it is between 0 and 100, print that the temperature is in the normal    #
# range.                                                                       #
# • If it is 100, print that the temperature is at the boiling point.          #
# • If it is above 100, print that the temperature is above the boiling point. #
################################################################################

temperature_in_celsius = eval(input("enter a temperature in celsius "))
if temperature_in_celsius < -273.15:
	print("temperature is invalid because it is below absolute zero")
elif temperature_in_celsius == -273.15:
	print("temperature is absolute 0")
elif temperature_in_celsius < 0 and temperature_in_celsius > -273.15:
	print("temperature is below freezing")
elif temperature_in_celsius == 0:
	print("temperature is at the freezing point")
elif temperature_in_celsius > 0 and temperature_in_celsius < 100:
	print("temperature is in the normal range")
elif temperature_in_celsius == 100:
	print("temperature is at the boiling point")
elif temperature_in_celsius > 100:
	print("the temperature is above the boiling point")