##############################################################################
# 10.                                                                        #
# Write a multiplication game program for kids. The program should give the  #
# player ten randomly generated multiplication questions to do. After each,  #
# the program should tell them whether they got it right or wrong and what   #
# the correct answer is.                                                     #
# Question 1: 3 x 4 = 12                                                     #
# Right!                                                                     #
# Question 2: 8 x 6 = 44                                                     #
# Wrong. The answer is 48.                                                   #
# ...                                                                        #
# ...                                                                        #
# Question 10: 7 x 7 = 49                                                    #
# Right.                                                                     #
##############################################################################
from random import randint


for i in range(1,11):
	random_number_1 = randint(1,10)
	random_number_2 = randint(1,10)
	print(random_number_1," x ",random_number_2,end="")
	user_answer_1 = eval(input("? "))
	correct_answer_1 = random_number_1*random_number_2
	if user_answer_1 == correct_answer_1:
		print("Right!")
	else:
		print("Wrong the answer is ",correct_answer_1)