###################################################################
# 2.                                                              #
#  Write a program to fill the screen horizontally and vertically #
#   with your name. [Hint: add the option end= '' into the print  #
#   function to fill the screen horizontally.]                    #
###################################################################
for i in range(50):
	print('yourname'*50, end='')