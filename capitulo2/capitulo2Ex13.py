##############################################################
# 13.                                                        #
# Use a for loop to print an upside down triangle like the   #
# one below. Allow the user to specify how high the triangle #
# should be.                                                 #
# ****                                                       #
# ***                                                        #
# **                                                         #
# *                                                          ##############################################################
# for i in range(eval(input('how high the triangle should be?')),-1):
for i in range(eval(input('how high the triangle should be?')),0,-1):
	print('*'*i)