#!/usr/bin/perl
############################################################
# 9. The Fibonacci numbers are the sequence below,         #
#  where the first two numbers are 1, and each number      #
#  thereafter is the sum of the two preceding numbers.     #
#   Write a program that asks the user how many Fibonacci  #
#   numbers to print and then prints that many.            #
# 	1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89 . . .              #
############################################################

use strict;
use warnings;
my $total_number = <STDIN>;

my $last_number = 1;
my $last_last_number =0;
my $output_string = "";
foreach my $toe(1..$total_number){
	my $new_fibonacci_number = $last_number+$last_last_number;
	$last_last_number = $last_number;
	$last_number = $new_fibonacci_number;
	$output_string .= $new_fibonacci_number.", ";
}
print $output_string."\n";


# last_number = 1
# last_last_number = 0
# output_string = 'x'
# for i in range(total_number-2):
# 	new_fibonacci_number = last_number + last_last_number
# 	last_number = new_fibonacci_number
# 	last_last_number = last_number
# 	output_string = output_string,new_fibonacci_number

