#########################################################
# 14.                                                   #
# Use for loops to print a diamond like the one below.  #
# Allow the user to specify how high the                #
# diamond should be.                                    #
#    *                                                  #
#   ***                                                 #
#  *****                                                #
# *******                                               #
#  *****                                                #
#   ***                                                 #
#    *                                                  #
#########################################################
diamond_size = eval(input('how high the triangle should be?'))
for i in range(0,diamond_size+1):
	z = diamond_size - i
	print(' '*(z),'*'*i,'*','*'*i,' '*z,sep='')

for i in range(diamond_size-1,-1,-1):
	z = diamond_size - i
	print(' '*(z),'*'*i,'*','*'*i,' '*z,sep='')