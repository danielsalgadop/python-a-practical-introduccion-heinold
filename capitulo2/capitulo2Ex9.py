###########################################################
# 9.                                                      #
#  The Fibonacci numbers are the sequence below,          #
#  where the first two numbers are 1, and each number     #
#  thereafter is the sum of the two preceding numbers.    #
#   Write a program that asks the user how many Fibonacci #
#   numbers to print and then prints that many.           #
# 	1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89 . . .             #
###########################################################
# USING Type conversion str()  not explained in book. To avoid a bunch of (((())))
total_number = eval(input("how many Fibonacci numbers to print?"))

# TODO:
# si total_number es menor a 2 hacer exit

last_number = 1
last_last_number = 0
output_string = 'x'
for i in range(total_number-2):
	new_fibonacci_number = last_number + last_last_number
	last_last_number = last_number
	last_number = new_fibonacci_number
	output_string = output_string + " " + str(new_fibonacci_number)
	# output_string = output_string , new_fibonacci_number # original without type conversion, with a bunch on (((())))

# DUDA Sale un '(' muy raro en output !!
print(output_string, sep='')