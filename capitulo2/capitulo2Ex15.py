#####################################################
# 15.                                               #
# Write a program that prints a giant letter A      #
# like the one below. Allow the user to specify how #
# large the letter should be.                       #
#       *                                           #
#      * *                                          #
#     *****                                         #
#    *     *                                        #
#   *       *                                       #
#####################################################
letter_size = eval(input('how large the letter should be?'))
for i in range(letter_size):
	num_spaces = letter_size -i
	print(' '*num_spaces,'*'*letter_size)