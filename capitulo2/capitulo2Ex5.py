##############################################################
# 5.                                                         #
#  Write a program that uses a for loop to print the numbers #
#   8, 11, 14, 17, 20, . . . , 83, 86, 89.                   #
##############################################################
sum = 0
for i in range(1,33):
    if i%3 == 0:
        print (i+5,end=", ")