###################################
# 1.                              #
# Print a box like the one below. #
# *******************             #
# *******************             #
# *******************             #
# *******************             #
###################################
print( '*******************')
print( '*******************')
print( '*******************')
print( '*******************')

###################################
# 2.                              #
# Print a box like the one below. #
# *******************             #
# *                               #
# *                               #
# *                               #
# *                               #
# *******************             #
###################################
print( '*******************')
print( '*')
print( '*')
print( '*')
print( '*')
print( '*******************')

########################################
# 3.                                   #
# Print a triangle like the one below. #
# *                                    #
# **                                   #
# ***                                  #
# ****                                 #
########################################
print( '*')
print( '**')
print( '***')
print( '****')

#####################################################################################
# 4.                                                                                #
# Write a program that computes and prints the result of# 512 − 282 / (47 * 48 + 5) #
# . It is roughly    0.1017                                                         #
#####################################################################################
print((512-282) / (47 * 48 + 5))


#################################################################################################
# 5.
#  Ask the user to enter a number. Print out the square of the number, but use the sep optional argument to print it out in a full sentence that ends in a period. Sample output is shown below.
# Enter a number: 5      #                                                                      #
# The square of 5 is 25. #                                                                      #
#################################################################################################
numero_usuario = eval(input( 'Ask the user to enter a number'))
print('El cuadrado de ',numero_usuario,' es ',numero_usuario*numero_usuario,'.', sep='')

###########
# 6.                                                                                                #
#  Ask the user to enter a number x . Use the sep optional argument to print out x , 2x , 3x , 4x , and 5x , each separated by three dashes, like below.
# Enter a number: 7
# 7---14---21---28---35
###########
numero_usuario = eval(input( 'Ask the user to enter a number'))
print(2*numero_usuario,3*numero_usuario,4*numero_usuario,5*numero_usuario, sep='---')

#####################################
# 7.                                                                                                                          #
# Write a program that asks the user for a weight in kilograms and converts it to pounds. There are 2.2 pounds in a kilogram. #
#####################################
kg_usuario = eval(input( 'Write a program that asks the user for a weight in kilograms '))
print('In pounds',kg_usuario*2.2)

#############################################################
# 8.                                                        #
# Write a program that asks the user to enter three numbers #
#  (use three separate input statements). Create variables  #
#   called total and average that hold the sum and average  #
#    of the three numbers and print out the values of total #
#    and average. #                                         #
#############################################################
first = eval(input( 'enter three numbers (use three separate input statements)1'))
second = eval(input( 'enter three numbers (use three separate input statements)2'))
third = eval(input( 'enter three numbers (use three separate input statements)3'))
total = first+second+third
average = total / 3
print('total',total,'average',average)

###############################################################
# 9.                                                          #
#  A lot of cell phones have tip calculators. Write one. Ask  #
#  the user for the price of the meal and the percent tip     #
#   they want to leave. Then print both the tip amount and    #
#   the total bill with the tip included. #                   #
###############################################################
price = eval(input( 'price of the meal?'))
percent = eval(input('percent tip they want to leave?'))
price_increment = (price*percent)/100
total_price = price + price_increment
print('Debes pagar ',total_price)

