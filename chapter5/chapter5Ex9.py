##############################################################################
# 9.                                                                         #
# Write a program to count how many integers from 1 to 1000 are not perfect  #
# squares, perfect cubes, or perfect fifth powers.                           #
##############################################################################

# for analized_number in range(1,1001):

from math import floor

for analized_number in range(1,35):

	print("================= analized_number",analized_number, "=================")
	# has analized_number perfect squares, poerfect cubes or perfect fifth powers?
	has_any_exponentation = 0
	# recorrer numeros inferiores a analized_number/2
	for posible_potential in range(floor((analized_number/2)),1,-1):
		# print ("posible_potential [",posible_potential,"]")
		if has_any_exponentation == 0:
			# analysis perfect square
			if posible_potential*posible_potential == analized_number or posible_potential*posible_potential*posible_potential == analized_number or posible_potential*posible_potential*posible_potential*posible_potential*posible_potential == analized_number:
				has_any_exponentation = 1
				# print("free of potential by 2 [",analized_number,"] posible_potential [",posible_potential,"]")

	if has_any_exponentation == 0:
		print("free of potential by 2,3,5 analized_number [",analized_number,"]")
	# else:
	# 	print("It has exponentation analized_number [",analized_number,"] especificamente en potencia de [",has_any_exponentation,"]")

		# if posible_potential*posible_potential != analized_number or posible_potential*posible_potential*posible_potential != analized_number or posible_potential*posible_potential*posible_potential*posible_potential*posible_potential != analized_number:
