##################################################################################
# 10.                                                                            #
# Ask the user to enter 10 test scores. Write a program to do the following:     #
# (a) Print out the highest and lowest scores.                                   #
# (b) Print out the average of the scores.                                       #
# (c) Print out the second largest score.                                        #
# (d) If any of the scores is greater than 100, then after all the scores have   #
# been entered, print a message warning the user that a value over 100 has been  #
# entered.                                                                       #
# (e) Drop the two lowest scores and print out the average of the rest of them.  #
##################################################################################
# test_number_1 = eval(input("please enter a number: "))
# test_number_2 = eval(input("please enter a number: "))
# test_number_3 = eval(input("please enter a number: "))
# test_number_4 = eval(input("please enter a number: "))
# test_number_5 = eval(input("please enter a number: "))
# test_number_6 = eval(input("please enter a number: "))
# test_number_7 = eval(input("please enter a number: "))
# test_number_8 = eval(input("please enter a number: "))
# test_number_9 = eval(input("please enter a number: "))
# test_number_10 = eval(input("please enter a number: "))

test_number_1 = 1
test_number_2 = 2
test_number_3 = 3
test_number_4 = 550
test_number_5 = 55
test_number_6 = 6
test_number_7 = 7
test_number_8 = 8
test_number_9 = 9
test_number_10 = 10


highest_score = 0
lowest_score = 0
average_score = 0
second_highest_score = 0

# test_number_1 is (in this moment) the lowest and highest score
highest_score = test_number_1
lowest_score = test_number_1

second_highest_score = highest_score


###########################
# determine highest_score #
###########################
if highest_score <= test_number_2:
	highest_score = test_number_2

if highest_score <= test_number_3:
	highest_score = test_number_3

if highest_score <= test_number_4:
	highest_score = test_number_4

if highest_score <= test_number_5:
	highest_score = test_number_5

if highest_score <= test_number_6:
	highest_score = test_number_6

if highest_score <= test_number_7:
	highest_score = test_number_7

if highest_score <= test_number_8:
	highest_score = test_number_8

if highest_score <= test_number_9:
	highest_score = test_number_9

if highest_score <= test_number_10:
	highest_score = test_number_10


###############################################################################
# determine second_highest_score                                              #
# if there is two (or more) highest scores with same value these methos will  #
# consider the next highest vale as second_highest_score                      #
###############################################################################

if test_number_1 < highest_score:
	second_highest_score = test_number_1

if test_number_2 > second_highest_score and test_number_2 < highest_score:
	second_highest_score = test_number_2

if test_number_3 > second_highest_score and test_number_3 < highest_score:
	second_highest_score = test_number_3

if test_number_4 > second_highest_score and test_number_4 < highest_score:
	second_highest_score = test_number_4

if test_number_5 > second_highest_score and test_number_5 < highest_score:
	second_highest_score = test_number_5

if test_number_6 > second_highest_score and test_number_6 < highest_score:
	second_highest_score = test_number_6

if test_number_7 > second_highest_score and test_number_7 < highest_score:
	second_highest_score = test_number_7

if test_number_8 > second_highest_score and test_number_8 < highest_score:
	second_highest_score = test_number_8

if test_number_9 > second_highest_score and test_number_9 < highest_score:
	second_highest_score = test_number_9

if test_number_10 > second_highest_score and test_number_10 < highest_score:
	second_highest_score = test_number_10


# Trying to identify second_highest_score diferent from highest_score even 
# wher there are more than one values whit highest_score
#
# flag_first_time_hightest_score = 0

# if highest_score == test_number_1:
# 	flag_first_time_hightest_score = 1
# if highest_score == test_number_2 and flag_first_time_hightest_score == 0:
# 	flag_first_time_hightest_score = 1
# 	second_highest_score = test_number_2

# if highest_score == test_number_3 and flag_first_time_hightest_score == 0:
# 	flag_first_time_hightest_score = 1
# 	second_highest_score = test_number_3
# if highest_score == test_number_4 and flag_first_time_hightest_score == 0:
# 	flag_first_time_hightest_score = 1
# 	second_highest_score = test_number_4

# if highest_score == test_number_5 and flag_first_time_hightest_score == 0:
# 	flag_first_time_hightest_score = 1
# 	second_highest_score = test_number_5

# if highest_score == test_number_6 and flag_first_time_hightest_score == 0:
# 	flag_first_time_hightest_score = 1
# 	second_highest_score = test_number_6

# if highest_score == test_number_7 and flag_first_time_hightest_score == 0:
# 	flag_first_time_hightest_score = 1
# 	second_highest_score = test_number_7

# if highest_score == test_number_8 and flag_first_time_hightest_score == 0:
# 	flag_first_time_hightest_score = 1
# 	second_highest_score = test_number_8

# if highest_score == test_number_9 and flag_first_time_hightest_score == 0:
# 	flag_first_time_hightest_score = 1
# 	second_highest_score = test_number_9

# if highest_score == test_number_10 and flag_first_time_hightest_score == 0:
# 	flag_first_time_hightest_score = 1
# 	second_highest_score = test_number_10



###########################
# determine lowest_score #
###########################
if lowest_score > test_number_2:
	lowest_score = test_number_2
if lowest_score > test_number_3:
	lowest_score = test_number_2
if lowest_score > test_number_4:
	lowest_score = test_number_4
if lowest_score > test_number_5:
	lowest_score = test_number_5
if lowest_score > test_number_6:
	lowest_score = test_number_6
if lowest_score > test_number_7:
	lowest_score = test_number_7
if lowest_score > test_number_8:
	lowest_score = test_number_8
if lowest_score > test_number_9:
	lowest_score = test_number_9
if lowest_score > test_number_10:
	lowest_score = test_number_10

###########################
# calculate average_score #
###########################
average_score = (test_number_1 + test_number_2 + test_number_3 + test_number_4 + test_number_5 + test_number_6 + test_number_7 + test_number_8 + test_number_9 + test_number_10) / 10



##############################
# Is there a value over 100? #
##############################

if test_number_1 > 100 or test_number_2 > 100 or test_number_3 > 100 or test_number_4 > 100 or test_number_5 > 100 or test_number_6 > 100 or test_number_7 > 100 or test_number_8 > 100 or test_number_9 > 100 or test_number_10 > 100:
	print("WARNING!! there is a value over 100 has been entered")

##################################################################################
# 10.                                                                            #
# Ask the user to enter 10 test scores. Write a program to do the following:     #
# (c) Print out the second largest score.                                        #
# (e) Drop the two lowest scores and print out the average of the rest of them.  #
##################################################################################

print("highest_score [",highest_score,"] lowest_score [",lowest_score,"] second_highest_score [",second_highest_score,"] average_score [",average_score,"]")