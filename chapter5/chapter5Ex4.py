############################################################################
# 4.                                                                       #
# Write a program to compute the sum 1 − 2 + 3 − 4 + · · · + 1999 − 2000 . #
############################################################################
result = 0
last_even_number = 0

for i in range(1,2001):
	if i%2 == 0: # it is an odd number
		result = result + i
	else:
		result = result - i

print("result [",result,"]")