###################################################################################
# 5.                                                                              #
# Write a program that asks the user to enter a number and prints the sum of the  #
# divisors of that number. The sum of the divisors of a number is an important    #
# function in number theory.                                                      #
###################################################################################
user_number = eval(input("enter a number: "))
divisor_sum = 0
for i in range(1,user_number):
	if user_number%i == 0:  # this is a divisor
		divisor_sum = divisor_sum + i

print("the divisor_sum of the user_number [",user_number,"] is ",divisor_sum)