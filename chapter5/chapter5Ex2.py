#################################################################################
# 2.                                                                            #
# Write a program that counts how many of the squares of the numbers from 1 to  #
# 100 end in a 4 and how many end in a 9.                                       #
#################################################################################
counter_number_ending_4 = 0
counter_number_ending_9 = 0
for i in range(101):
	if (i**2)%10==4:
		counter_number_ending_4 = counter_number_ending_4 + 1
	elif (i**2)%10==9:
		counter_number_ending_9 = counter_number_ending_9 + 1
print("counter_number_ending_4 [",counter_number_ending_4,"]")
print("counter_number_ending_9 [",counter_number_ending_9,"]")