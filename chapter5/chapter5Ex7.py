##################################################################################
# 7.                                                                             #
# An integer is called squarefree if it is not divisible by any perfect squares  #
# other than 1. For instance, 42 is squarefree because its divisors are          #
# 1, 2, 3, 6, 7, 21, and 42, and none of those numbers (except 1) is a perfect   #
# square. On the other hand, 45 is not squarefree because it is divisible by 9,  #
# which is a perfect square. Write a program that asks the user for an integer   #
# and tells them if it is squarefree or not.                                     #
##################################################################################
user_number = eval(input("enter a number: "))
for is_this_square_free in range(1,user_number+1):
	flag_has_squared_divisors = 0
	for posible_divisor in range(1,is_this_square_free):
		if is_this_square_free%posible_divisor == 0:  # this is a divisor
			# has this divisor made by perfect squares?
			for posible_square in range(2,posible_divisor):
				if posible_square*posible_square == posible_divisor:
					flag_has_squared_divisors = 1

print("Is this a squarefree? [",is_this_square_free,"]\t",end="")
if flag_has_squared_divisors == 1:
	print("No, it is not")
else:
	print("Yes, is is")