###################################################################################
# 6.                                                                              #
# A number is called a perfect number if it is equal to the sum of all of its     #
# divisors, not including the number itself. For instance, 6 is a perfect number  #
# because the divisors of 6 are 1, 2, 3, 6 and 6 = 1 + 2 + 3 . As another         #
# example, 28 is a perfect number because its divisors are 1, 2, 4,7, 14, 28 and  #
# 28 = 1 + 2 + 4 + 7 + 14 . However, 15 is not a perfect number because its       #
# divisors are 1, 3, 5, 15 and 15 = 1 + 3 + 5 . Write a program that finds all    #
# four of the perfect numbers that are less than 10000.                           #
###################################################################################

for is_this_perfect_number in range(1,10000):
	sum_of_divisors = 0
	for posible_divisor in range(1,is_this_perfect_number):
		if is_this_perfect_number%posible_divisor == 0:  # this is a divisor
			sum_of_divisors = sum_of_divisors + posible_divisor
	if sum_of_divisors == is_this_perfect_number: # it is a perfect number if sum_of_divisors is equal to de value of is_this_perfect_number
		print("this is a perfect number [",is_this_perfect_number,"] sum_of_divisors [",sum_of_divisors,"]")