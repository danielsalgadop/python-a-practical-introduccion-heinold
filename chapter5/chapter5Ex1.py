#################################################################################
# 1.                                                                            #
# Write a program that counts how many of the squares of the numbers from 1 to  #
# 100 end in a 1.                                                               #
#################################################################################
counter_number_ending_1 = 0
for i in range(101):
	if (i**2)%10==1:
		counter_number_ending_1 = counter_number_ending_1 + 1
print("counter_number_ending_1 [",counter_number_ending_1,"]")