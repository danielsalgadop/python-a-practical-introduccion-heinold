#################################################################################
# 11.                                                                           #
# Write a program that computes the factorial of a number. The factorial, n! ,  #
# of a number n is the product of all the integers between 1 and n , including  #
# n . For instance, 5! = 1 x 2 x 3 x 4 x 5 = 120 .[Hint: Try using a            #
# multiplicative equivalent of the summing technique.]                          #
#################################################################################
number_to_factorize = eval(input("please entere number_to_factorize: "))
calculated_factorization = 1
for i in range(number_to_factorize,1,-1):
	calculated_factorization = calculated_factorization * i
	print(i)

print("calculated_factorization [",calculated_factorization,"]")

##############################################
# simple test                                #
# compare my result to math.factorial result #
##############################################
from math import factorial
if(factorial(number_to_factorize) != calculated_factorization):
	print("FATAL ERROR, factorization not correct")
