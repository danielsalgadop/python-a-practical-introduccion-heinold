##########################################################################################
# 14.                                                                                    #
# This exercise is about the well-known Monty Hall problem. In the problem, you          #
# are a contestant on a game show. The host, Monty Hall, shows you three doors.          #
# Behind one of those doors is a prize, and behind the other two doors are               #
# goats. You pick a door. Monty Hall, who knows behind which door the prize              #
# lies, then opens up one of the doors that doesn’t contain the prize. There are         #
# now  two doors left, and Monty gives you the opportunity to change your choice.        #
# Should you keep the same door, change doors, or does it not matter?                    #
# (a) Write a program that simulates playing this game 10000 times and                   #
# calculates what percentage of the time you would win if you switch and what            #
# percentage of the time you would win by not switching.                                 #
# (b) Try the above but with four doors instead of three. There is still only one        #
# prize, and Monty still opens up one door and then gives you the opportunity to switch. #
##########################################################################################
from random import randint
total_tries = 10000
counter_wining_games_not_changing = 0
counter_wining_games_changing = 0

for i in range(1,total_tries + 1):
	random_wining_gate = randint(1,3)
	guessed_gate = randint(1,3)
	# print("\n=======================\nrandom_wining_gate [",random_wining_gate,"] guessed_gate [",guessed_gate,"]")

	#######################
	# not changing choice #
	#######################
	if random_wining_gate == guessed_gate:
		counter_wining_games_not_changing = counter_wining_games_not_changing + 1
		# print("wining NOT changing")

	###################
	# changing choice #
	###################
	# Monty Hall opens door number 3
	# random_wining_gate 2
	if guessed_gate == 1 and random_wining_gate == 2:
		guessed_gate = 2
	elif guessed_gate == 2 and random_wining_gate == 2:
		guessed_gate = 1
	# random_wining_gate 1
	elif guessed_gate == 2 and random_wining_gate == 1:
		guessed_gate = 1
	elif guessed_gate == 1 and random_wining_gate == 1:
		guessed_gate = 2

	# Monty Hall opens door number 2
	# random_wining_gate 1
	elif guessed_gate == 3 and random_wining_gate == 1:
		guessed_gate = 1
	elif guessed_gate == 1 and random_wining_gate == 1:
		guessed_gate = 3
	# random_wining_gate 3
	elif guessed_gate == 3 and random_wining_gate == 3:
		guessed_gate = 1
	elif guessed_gate == 1 and random_wining_gate == 3:
		guessed_gate = 3

	# Monty Hall opens door number 1
	# random_wining_gate 2
	elif guessed_gate == 1 and random_wining_gate == 2:
		guessed_gate = 3
	elif guessed_gate == 3 and random_wining_gate == 2:
		guessed_gate = 1
	# random_wining_gate 3
	elif guessed_gate == 3 and random_wining_gate == 3:
		guessed_gate = 2
	elif guessed_gate == 1 and random_wining_gate == 3:
		guessed_gate = 3

	# changing choice
	# print("Changed guessed_gate [",guessed_gate,"]")
	if random_wining_gate == guessed_gate:
		counter_wining_games_changing = counter_wining_games_changing + 1
		# print("wining")
	# print("not wining")

###########
# results #
###########
print("NOT CHANGING wining percentage [",counter_wining_games_not_changing*100/total_tries,"%]")
print("CHANGING wining percentage [",counter_wining_games_changing*100/total_tries,"%]")