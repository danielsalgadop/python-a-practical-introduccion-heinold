###################################################################################
# 8.                                                                              #
# Write a program that swaps the values of three variables x , y , and z , so     #
# that x gets the value of y , y gets the value of z , and z gets the value of x  #
###################################################################################
x = "x_value"
y = "y_value"
z = "z_value"
print("values BEFORE x [",x,"] y [",y,"] z [",z,"]")
x,y,z = y,z,x
print("values AFTER x [",x,"] y [",y,"] z [",z,"]")
