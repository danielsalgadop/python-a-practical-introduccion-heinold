##############################################################################
# 3.                                                                         #
# Write a program that asks the user to enter a value n , and then computes  #
# (1 + (1/2) + (1/3) + ... + (1/n)) -ln(n)                                   #
# The ln function is log in the math module.                                 #
##############################################################################
from math import log

n_value = eval(input("enter a value: "))

result = 1
for i in range(2,n_value):
	result = result + (1/i)

result = result - log(n_value)
print("result [",result,"]")