##################################################################################
# 12.                                                                            #
# Write a program that asks the user to guess a random number between 1 and 10.  #
# If they guess right, they get 10 points added to their score, and they lose 1  #
# point for an incorrect guess. Give the user five numbers to guess and print    #
# their score after all the guessing is done.                                    #
##################################################################################
from random import randint
user_results = 0
for i in range (5):
	# generate random number
	random_number = randint(1,10)
	# ask for number
	users_guess = eval(input("please guess a number: "))

	# compare
	if(random_number == users_guess):
		print("well, done (+10)")
		user_results = user_results + 10
	else:
		print("Incorrect! it was [",random_number,"] (-1)")
		user_results = user_results - 1


print("Your result is [",user_results,"]")