##################################################################################
# 13.                                                                            #
# In the last chapter there was an exercise that asked you to create a           #
# multiplication game for kids. Improve your program from that exercise to keep  #
# track of the number of right and wrong answers. At the end of the program,     #
# print a message that varies depending on how many questions the player got     #
# right.                                                                         #
##################################################################################
from random import randint
correct_answers = 0
for i in range(1,11):
	random_number_1 = randint(1,10)
	random_number_2 = randint(1,10)
	print(random_number_1," x ",random_number_2,end="")
	user_answer_1 = eval(input("? "))
	correct_answer_1 = random_number_1*random_number_2
	if user_answer_1 == correct_answer_1:
		correct_answers = correct_answers + 1
		print("Right!")
	else:
		print("Wrong the answer is ",correct_answer_1)

#################
# print Results #
#################
if correct_answers < 5:
	print("you failed!, keep trying")
elif correct_answers == 10:
	print("Brilliant, that was PERFECT!")
else:
	print("well done, you know how to multiplicate")